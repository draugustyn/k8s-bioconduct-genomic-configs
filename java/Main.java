package com.company;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

/*
/*   Simple Sample Java Client for checking  of running Rserve with AMARETTO package
/*

 */
public class Main {

    static String  Host =  "35.205.254.231" ; // set IP of externalized bio-world-api LoadBalancer service
    static Integer  Port = 6311 ; // default port

    public static void main(String[] args) {
	// write your code here

         simple_demo() ;
         amaretto_demo();
    }

    private static void simple_demo()
        {

        RConnection connection = null;
        try {
            connection = new RConnection ( Host, Port);
            String vector = "c(1,2,3,4)" ;
            connection.eval("meanVal = mean(" + vector + ")");
            double mean = connection.eval("meanVal").asDouble();
            System.out.println (mean);

         } catch (RserveException | REXPMismatchException ex)
            {
                ex.printStackTrace();
            }
    }


    private static void amaretto_demo()
    {

        RConnection connection = null;
        String res ;

        try {
            connection = new RConnection ( Host, Port);

            res = connection.eval("library (AMARETTO)").asString();
            System.out.println (res);
            if (res.equals("AMARETTO")){
                System.out.println ("the remote package is available");
            }

            /*
            res = connection.eval ("try ({\n" +
                    "      library (AMARETTO)\n" +
                    "      data('ProcessedDataLIHC')\n" +
                    "      AMARETTOinit <- AMARETTO_Initialize(ProcessedData = ProcessedDataLIHC, NrModules = 2, VarPercentage = 50)\n" +
                    "      AMARETTOresults <- AMARETTO_Run(AMARETTOinit)\n" +
                    "      c (AMARETTOresults, Sys.info()[\"nodename\"]) # result\n" +
                    "    })\n").asString();
            */
        } catch (RserveException | REXPMismatchException ex)
        {
            ex.printStackTrace();
        }
    }

}
