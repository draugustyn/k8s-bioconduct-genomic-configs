# k8s-biocoduct-genomic-configs - the demo project

## Demo project for using kubernetes to externalize (in a cloud) the functionality of genomic package - Amaretto - using docker image based on bioconductor one

Thanks to:

Shinde J, Everaert C, Bakr S, Nabian M, Xu J, Carey V, Pochet N, Gevaert O (2020). AMARETTO: Regulatory Network Inference and Driver Gene Evaluation using Integrative Multi-Omics Analysis and Penalized Regression. R package version 1.6.0.

http://www.bioconductor.org/packages/release/bioc/html/AMARETTO.html




https://www.bioconductor.org/help/docker/

https://github.com/Bioconductor/bioconductor_docker

Huber et al., 2015 Nature Methods 12:115-121
https://www.nature.com/articles/nmeth.3252


Solution based on bioconductor_docker and  installed.packages:

    AMARETTO    http://www.bioconductor.org/packages/release/bioc/html/AMARETTO.html 
    Rserve      https://www.rforge.net/Rserve/
    RSclient    https://cran.r-project.org/web/packages/RSclient/RSclient.pdf

# Running in Docker 

## Server side (running Rserve)

1: create docker conatainer
 
 docker run -e PASSWORD=bioc  -p **8786**:8787 -p **6311**:6311 amarebioconductor:5.0

2: starting RSession in local beb browser

[http://localhost:**8786**/](url)

3: enable Rserver (expose R functionality)

library(Rserve)
r =  Rserve (args = " --vanilla --RS-enable-remote", port = **6311**)

## Client side (usage amarebiocoductor image functionality from local R outside the container) 

library (RSclient)

c <- RS.connect("localhost", **6311**)

RS.eval(c,  quote(   Sys.info()["nodename"]  ) ) # the name of remote host

RS.eval(c,  quote( library (AMARETTO) ))  # loading sample genomic package

RS.eval(c,  quote( AMARETTOinit <- AMARETTO_Initialize(ProcessedData = ProcessedDataLIHC, NrModules = 2, VarPercentage = 50)))

RS.eval(c,  quote( AMARETTOresults <- AMARETTO_Run(AMARETTOinit)))

RS.close(c)


# Running in Kubernetes

**biodep.yaml**   - (deployment) allows to create pods/container with RStudio web console with additional functionality  (RSClient, RSServer, Bioconductor, Amaretto)

**bioserv.yaml** -  (service) allows to create services (Cluster IP/LoadBalancer) to expose RStudio  internally (inside the cluster) and externally (outside from the cloud).  Service for session-base connections.

**biodep-rserv.yaml**  -  (deployment) allows to create headless pods/container providing remote R functionality by RServer (with Amaretto and Bioconductor also)

**bioserv-rserv.yaml**  -  (service) allows to create services (Cluster IP/LoadBalancer) that  enables remote Rserver functionality  for e.g.:  internal RStudio (inside the cluster) or external java-app (outside from the cloud).  Service for state-less invoking.




